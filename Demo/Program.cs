﻿using MVC;
using NetWebMVC.Web.Config;
using NetWebMVC.Web.Controller;
using System;
using System.Windows.Forms;

namespace NetWebMVC
{
    class Program
    {

        static void Main(string[] args)
        {
            IHttpServer httpserver = new IHttpServer(new CheckUser());
            //路径,控制器,视图目录,是否拦截(默认true)
            httpserver.Route.Add("", new IndexController(), "", false);
            httpserver.Route.Add("Home", new HomeController(), "Home");
            httpserver.Start();
        }
    }
}
