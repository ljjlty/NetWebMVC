﻿
using Newtonsoft.Json.Linq;
using System;
using Model;
using NetWebMVC.Web.Config;

using MVC;

namespace NetWebMVC.Web.Controller
{
    class IndexController : MVC.Controller
    {
        //输出登录页面
        public void Index()
        {
            string c = Body();
            string s = InputById(1);
            string ip1 = Input("ip");
            string ip = Request.Headers.ToString();
               Show("login");
           // ShowText(ip);
        }

        //打印字符串
        public void hello()
        {
            ShowText("hello");
        }
        //get方法参数方式获取请求参数
        public void Say(string name, int age, float a, double b, DateTime dd)
        {
            string s = InputById(1);
            string s1 = Input("name");
            ShowText(name + "age:" + age + "a:" + a + "b:" + b + "date:" + dd.ToShortDateString());
        }
        //输出json格式
        public void Show()
        {
            var list = DB.Context.From<tb_users>().ToDataTable();
            ShowJSON(list);
        }
        //用户登录检查方法
        public void check()
        {
            string txt = Body();
            string username = Input("username");
            string vcode = Input("vcode");
            if (Session.Get("code") != null)
            {

                string vcode_ = Session.Get("code").ToString();
            }
            JObject jo = new JObject();
            jo["code"] = 0;
            jo["message"] = "success";
            ShowJSON(jo);
        }
        [IntercptMethod(true)]//控制器方法拦截器 此处表示拦截Main方法
        public void Main()
        {
            SetAttr("realname", "joman");
            Show("main");
        }
        //文件上传接受函数
        public void upimage()
        {
            string filename = Files().FileName;
            Files().SaveFile("D:\\" + filename);
            ShowText("ok");
        }
        //输出验证码方法
        public void verifycode()
        {
            string code;
            byte[] jpg = getVCode(out code);
            Session.Set("code", code);            
            ShowStream(jpg, "image/jpeg");
        }


       
    }
}
