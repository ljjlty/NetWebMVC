﻿
using DeviceCenter.Config;
using Dos.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceCenter.Controller
{
    internal class BaseController:MVC.Controller
    {
        public static readonly DbSession Db = DataBase.Db;
    }
}
