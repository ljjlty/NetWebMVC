﻿using Dos.ORM;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceCenter.Controller
{
    internal class IndexController : BaseController
    {
        public void index()
        {

            Show("index");
        }
        public void main()
        {
            JArray ja = new JArray();
            for (int i = 0; i < 10; i++)
            {
                JObject jo = new JObject();
                jo["id"] = 1;
                jo["menuname"] = "菜单" + i.ToString();
                jo["url"] = "#";
                ja.Add(jo);
            }
            SetAttr("menuls", ja);
            SetAttr("username", "测试");
            Show("main");
        }
        public void getdata()
        {

            var ds = Db.FromSql("select username,realname from tb_users").ToDataTable();
            ShowJSON(ds);
        }
    }
}
