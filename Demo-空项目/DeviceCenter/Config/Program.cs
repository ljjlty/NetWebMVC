﻿using DeviceCenter.Config;
using DeviceCenter.Controller;
using MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceCenter
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "MVC项目";
            WSocket.Socket.start();
            IHttpServer httpserver = new IHttpServer(new CheckUser());
            httpserver.Route.Add("", new IndexController(), "", false);
            httpserver.Start();
        }
    }
}
