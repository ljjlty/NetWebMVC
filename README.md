	namespace NetWebMVC
	{
		class Program
		{
			static void Main(string[] args)
			{

				IHttpServer httpserver = new IHttpServer(new UserCheck());				
				httpserver.Roule.Add("", new IndexController(), "", false); //路径,控制器,视图目录,是否拦截(默认true)
				httpserver.Roule.Add("Home", new HomeController(), "Home");
				httpserver.Start();
			}
		}
	}

	config.json参数配置
	{
		{
		"Server": {
			"Port": "8004",
			"Compress": "deflate"
		},	
		"Config": {
			"AppName": "",
			"WebRoot": "WebRoot",
			"template": "View",
			"template_type": ".html",
			"open_cache": true,
			"cache_max_age": "315360000",
			"document_charset": "utf-8",
			"SessonName": "__guid_session",
			"session_timer": 60,
			"Session_open": true,
			"open_debug": true,
			"directory": [
			{
				"path": "/view/data/",
				"permission": false
			}
			]
		}
	}
	mime.json配置
	[
	{
		"Extensions": "css",
		"MimeType": "text/css"
	},
	{
		"Extensions": "html;htm",
		"MimeType": "text/html"
	},
	{
		"Extensions": "js",
		"MimeType": "text/javascript"
	},
	{
		"Extensions": "jpeg;jpg",
		"MimeType": "image/jpeg"
	},
	{
		"Extensions": "png",
		"MimeType": "image/x-png"
	},
	{
		"Extensions": "ico",
		"MimeType": "image/x-icon"
	},
	{
		"Extensions": "gif",
		"MimeType": "image/gif"
	},
	{
		"Extensions": "xml",
		"MimeType": "text/xml"
	},
	{
		"Extensions": "svg",
		"MimeType": "image/svg+xml"
	},
	{
		"Extensions": "woff",
		"MimeType": "application/font-woff"
	},
	{
		"Extensions": "woff2",
		"MimeType": "application/font-woff2"
	},
	{
		"Extensions": "rtx",
		"MimeType": "text/richtext"
	},
	{
		"Extensions": "zip",
		"MimeType": "application/x-zip-compressed"
	},
	{
		"Extensions": "txt",
		"MimeType": "text/plain"
	},
	{
		"Extensions": "svg;svgz",
		"MimeType": "image/svg+xml"
	},
	{
		"Extensions": "apk",
		"MimeType": "application/vnd.android.package-archive"
	}
	]

	IndexController 控制器说明
	

	namespace NetWebMVC.Web.Controller
	{
		class IndexController : MVC.Command.Controller
		{
			//输出登录页面
			public void Index()
			{
				ShowHTML("login");
			}

			//打印字符串
			public void hello()
			{
				ShowText("hello");
			}
			//get方法参数方式获取请求参数
			public void Say(string name, int age, float a, double b, DateTime dd)
			{
				string s = InputById(1);
				string s1 = Input("name");
				ShowText(name + "age:" + age + "a:" + a + "b:" + b + "date:" + dd.ToShortDateString());
			}
			//输出json格式
			public void Show()
			{
				var list = DB.Context.From<tb_users>().ToDataTable();
				ShowJSON(list);
			}
			//用户登录检查方法
			public void check()
			{
				string txt = Content();
				string username = Input("username");
				string vcode = Input("vcode");
				if (Session.Get("code") != null)
				{

					string vcode_ = Session.Get("code").ToString();
				}
				JObject jo = new JObject();
				jo["code"] = 0;
				jo["message"] = "success";
				ShowJSON(jo);
			}
			[IntercptMethod(true)]//控制器方法拦截器 此处表示拦截Main方法
			public void Main()
			{
				SetAttr("realname", "joman");
				ShowHTML("main");
			}
			//文件上传接收函数
			public void upimage()
			{
				string filename = Files().FileName;
				Files().SaveFile("D:\\" + filename);
				ShowText("ok");
			}
			//输出验证码方法
			public void verifycode()
			{
				string code = CreateRandomCode(4);
				Session.Set("code", code);
				byte[] jpg = CreateValidateGraphic(code);
				ShowStream(jpg, "image/jpeg");
			}

			/// <summary>
			/// 生成随机的字符串
			/// </summary>
			/// <param name="codeCount"></param>
			/// <returns></returns>
			public string CreateRandomCode(int codeCount)
			{
				string allChar = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,a,b,c,d,e,f,g,h,i,g,k,l,m,n,o,p,q,r,F,G,H,I,G,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,s,t,u,v,w,x,y,z";
				string[] allCharArray = allChar.Split(',');
				string randomCode = "";
				int temp = -1;
				Random rand = new Random();
				for (int i = 0; i < codeCount; i++)
				{
					if (temp != -1)
					{
						rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
					}
					int t = rand.Next(35);
					if (temp == t)
					{
						return CreateRandomCode(codeCount);
					}
					temp = t;
					randomCode += allCharArray[t];
				}
				return randomCode;
			}

			/// <summary>
			/// 创建验证码图片
			/// </summary>
			/// <param name="validateCode"></param>
			/// <returns></returns>
			public byte[] CreateValidateGraphic(string validateCode)
			{
				Bitmap image = new Bitmap((int)Math.Ceiling(validateCode.Length * 16.0), 27);
				Graphics g = Graphics.FromImage(image);
				try
				{
					//生成随机生成器
					Random random = new Random();
					//清空图片背景色
					g.Clear(Color.White);
					//画图片的干扰线
					for (int i = 0; i < 25; i++)
					{
						int x1 = random.Next(image.Width);
						int x2 = random.Next(image.Width);
						int y1 = random.Next(image.Height);
						int y2 = random.Next(image.Height);
						g.DrawLine(new Pen(Color.Silver), x1, x2, y1, y2);
					}
					Font font = new Font("Arial", 13, (FontStyle.Bold | FontStyle.Italic));
					LinearGradientBrush brush = new LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Blue, Color.DarkRed, 1.2f, true);
					g.DrawString(validateCode, font, brush, 3, 2);

					//画图片的前景干扰线
					for (int i = 0; i < 100; i++)
					{
						int x = random.Next(image.Width);
						int y = random.Next(image.Height);
						image.SetPixel(x, y, Color.FromArgb(random.Next()));
					}
					//画图片的边框线
					g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);

					//保存图片数据
					MemoryStream stream = new MemoryStream();
					image.Save(stream, ImageFormat.Jpeg);

					//输出图片流
					return stream.ToArray();
				}
				finally
				{
					g.Dispose();
					image.Dispose();
				}
			}
		}
	}
